;; surely emacs has a proper mechanism for this, but use this for now,
;; just want to make sure all messages are printed with this function
(defun my-trace (msg)
  "for now just concatenate INFO with given message"
  (message (concat "INFO: " msg)))

(defun my-switch-to-last-buffer ()
  (interactive)
  (switch-to-buffer nil))

;; function to just execute a code block but without polluting global settings
;; TODO make it less stupid
(defun pls-run-and-show ()
  "execute source block without prompt and display image, requires plantuml for uml"
  ;; TODO is adding other drawing mechanisms?
  (interactive)
  (if (boundp 'org-plantuml-jar-path)
    ((setq org-confirm-babel-evaluate nil) ; because YOLO
     (org-babel-execute-src-block)
     (org-redisplay-inline-images)
     (setq org-confirm-babel-evaluate t))
    (my-trace "Plantuml not setup!")))

;; very nice function I've found here 
;; https://emacs.stackexchange.com/questions/48683/open-org-mode-links-to-info-in-different-window
;; should open any link in a new window
(defun my-org-info-open-new-window (path)
  "Open info in a new buffer"
  (let* ((available-windows (delete (selected-window) (window-list)))
         (new-window (or (car available-windows)
                         (split-window-sensibly)
                         (split-window-right))))
        (select-window new-window)
        (org-info-follow-link path)))

;; taken from 
;; https://stackoverflow.com/questions/9983731/emacs-and-ansi-term-elisp-iterate-through-a-list-of-buffers
;; not the best but works fine, definitely tear apart and make better
(defun my-cycle-ansi-term ()
  "cycle through buffers whose major mode is term-mode"
  (interactive)
  (when (string= "term-mode" major-mode)
    (bury-buffer))
  (let ((buffers (cdr (buffer-list))))
    (while buffers
      (when (with-current-buffer (car buffers) (string= "term-mode" major-mode))
        (switch-to-buffer (car buffers))
        (setq buffers nil))
      (setq buffers (cdr buffers)))))

;; Source: http://www.emacswiki.org/emacs-en/download/misc-cmds.el
(defun pls-revert-buffer ()
  "Revert buffer without confirmation."
  (interactive)
  (revert-buffer :ignore-auto :noconfirm))
 
(defun my-open-notes-file ()
  "check if notes file is declared and open it"
  (interactive)
  (if (boundp 'my-current-notes-file)
      (if (file-exists-p my-current-notes-file)
	  (find-file my-current-notes-file)
	(my-trace (concat "notes file not found at: " my-current-notes-file)))
    (my-trace "notes file not defined!")))

(defun my-reset-notes-file ()
  "sets notes file back to default"
  (interactive)
  (setq my-current-notes-file my-notes-file))

(defun my-set-current-buffer-as-notes-file ()
  "sets currently viewed file as current notes file (if the file is in orgmode)"
  (interactive)
  (when (buffer-file-name)
    (if (string-equal major-mode "org-mode")
	(setq my-current-notes-file (buffer-file-name))
      (my-trace "File not an orgmode file"))))

;; TODO
;;(defun my-magit-find-in-worktree (file)
  ;;(interactive)
  ;;(magit-find-file "{worktree}" file))

  
(provide 'my-utils)
