;; Collection of web pages where useful configs can be stolen from:
;; https://jamiecollinson.com/blog/my-emacs-config/
;; https://nextlevel-blog.de/emacs-for-academia-and-developers/
;; orgmode setup:
;; https://zzamboni.org/post/beautifying-org-mode-in-emacs/

;; I like this one, claims no config or project needed:
;; https://github.com/jacktasia/dumb-jump

;; setup org-download for easy image downloading

;; holy crap http://ehneilsen.net/notebook/orgExamples/org-examples.html#sec-18

;; use moody for nicer modeline
;; https://github.com/tarsius/moody
;; (or at least steal the spacemacs one)

;; use 
;; https://stackoverflow.com/questions/17900889/switch-between-frames-by-number-or-letter
;; to switch between multiple frames

;; BIG todo is switching buffers, but in a way that just jumps to
;; proper window/frame if buffer is already opened in it

;; TODOS:
;; - expand region, looks very useful
;; - org babel - for literate programming in orgmode
;; - pretty much everything on the above links
;; current WIP for work stuff:
;; - setup working with projects and compilers for C++
;; - get some cross-file permanent bookmark plugin when working with a
;; lot of files
;; - define keymapping to designate/jump to notes file

;; need this to get projectile find file working for big repos
;; https://stackoverflow.com/questions/32513593/emacs-helm-projectile-slow-startup-and-projectile-file-find-very-slow

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;; initialization and packages ;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; start the emacs server once
(load "server")
(unless (server-running-p) (server-start))

;; remember where all the configs are
(setq my-config-path
      (expand-file-name (concat user-emacs-directory "lisp/")))

;; add .emacs.d/lisp to load path, not sure if this is OS agnostic
(add-to-list 'load-path my-config-path)

;; add library for package manipulation
(require 'package)

;; add melpa repo to list of all package archives
(add-to-list 'package-archives
    '("melpa" . "https://melpa.org/packages/"))

;; initialize package module
(package-initialize)

;; refresh package archives
(unless package-archive-contents
  (package-refresh-contents))

;; not sure if this should always be called on init or just once but
;; this works nicely for now

;; this sets a file to store custom variables in use this so paths
;; never get pushed accidentally when they are set as defaults
;; (because the defaults are then written to THIS file and get pushed)
(customize-set-variable 'custom-file "my-custom-file.el")

;; I've moved all variables to setq, no default file should be needed
;; TODO revisit this, doesn't seem to be true :D

;; this sets the EXPLICITLY INSTALLED package list
(customize-set-variable 'package-selected-packages '(use-package
                                                      ;; general utils
                                                      evil
                                                      evil-leader
                                                      evil-escape
                                                      which-key
                                                      undo-tree
                                                      helm
                                                      ;; https://github.com/snosov1/toc-org
                                                      toc-org ; generate table of contents
                                                      ;; themes
                                                      nova-theme
                                                      ubuntu-theme
                                                      flatland-theme
                                                      zeno-theme
                                                      solarized-theme
                                                      ;; programming utils
                                                      projectile
                                                      helm-projectile
                                                      neotree
                                                      magit
                                                      rainbow-delimiters
                                                      ;; language server
                                                      flycheck
                                                      company
                                                      lsp-mode
						      sublimity
                                                      ))

;; make a theme toggler that goes through this list and sets the theme
;;(setq 'my-themes '(nova ubuntu flatland zeno solarized-gruvbox-dark))

;; automatically check above package list and install missing
(package-install-selected-packages)
;; prune unused packages
(package-autoremove)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;; package initialization ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; note on use-package: :ensure t will auto-install packages
;; listed. Since I prefer an explicit list of packages I will not use
;; :ensure t to download packages

;; import the use-package module
(require 'use-package)

;; import my own utility file with some extra functions
(require 'my-utils)

;; get some configuration paths from a local file
;; I use this for work specific configs that shouldn't be uploaded
(let ((local-config-file (concat my-config-path "local-config.el")))
  (if (file-exists-p local-config-file)
      (load-file local-config-file)
    (progn (my-trace "No local config file found, missing paths!")
	   (setq my-local-file-loaded nil))))

;; variables to define in this file (otherwise stuff will not work):
;; my-default-notes-file, my-current-notes-file, my-notes-folder
;; org-plantuml-jar-path, should really set the defaults above

;;

(use-package sublimity
  :init
  (use-package sublimity-map)
  (sublimity-mode 0)
  (setq sublimity-scroll-weight 5
	sublimity-scroll-drift-length 10)
  (setq sublimity-map-size 30)
  (setq sublimity-map-fraction 0.3)
  (setq sublimity-map-text-scale -7)
  (sublimity-map-set-delay nil)
  (add-hook 'sublimity-map-setup-hook
	    (lambda ()
	      (setq buffer-face-mode-face '(:family "Monospace"))
	      (buffer-face-mode))))

;; TODO should be creating a file that holds all relevant paths for
;; working with emacs and filling it up if emacs starts without one,
;; like, interactively asking for paths needed for the config to work

;; enable undo tree
(use-package undo-tree
  :init
  (global-undo-tree-mode))

;; projectile has its own map, and so do the above things probably :D
;; I still prefer my own maps done from scratch, but just so I don't forget
;;(define-key projectile-mode-map (kbd "p") 'projectile-command-map)

;; setup company
(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode))

;; TODO use package can be used inside another package as well
;; switch to using it like this, should be much nicer

;; set leader key BEFORE enabling evil
(use-package evil-leader
  :init
  (global-evil-leader-mode)
  :config
  (evil-leader/set-leader "<SPC>")
    ;; TODO fix keymap naming, make more uniform

  ;; prepare major mode keymaps for everything
  (setq my-elisp-major-mode-map (make-sparse-keymap)
        my-cpp-major-mode-map (make-sparse-keymap)
        my-haskell-major-mode-map (make-sparse-keymap)
        my-org-major-mode-map (make-sparse-keymap)
        my-window-keymap (make-sparse-keymap)
        my-file-keymap (make-sparse-keymap)
        my-buffer-keymap (make-sparse-keymap)
        my-projectile-mode-map (make-sparse-keymap)
        my-org-link-map (make-sparse-keymap) ; for links in orgmode
        my-magit-mode-map (make-sparse-keymap))

  ;; much nicer, this thing (bind-keys) can create prefixes as well
  ;; TODO see how these work with evil, there's probably a nicer way
  ;; so many options ffs

  ;; magit keymap
  ;; TODO this is crap, revisit
  (bind-keys :map my-magit-mode-map
             ("t" . magit-stage) ; t as in "this"
             ("a" . magit-stage-file) ; a as in "add"
             ("s" . magit-status)
	     ("f" . magit-find-file))

  ;; window manipulation keymap
  (bind-keys :map my-window-keymap
             ("f" . toggle-frame-fullscreen) ; TODO find better place for this
             ("k" . evil-window-delete)
             ("-" . split-window-below)
             ("/" . split-window-right)
             ;("l" . evil-window-move-far-right)
             ;("h" . evil-window-move-far-left)
             ;("k" . evil-window-move-very-top)
             ;("j" . evil-window-move-very-bottom)
             ("t" . my-cycle-ansi-term))

  ;; projectile keymap
  (bind-keys :map my-projectile-mode-map
             ("f" . helm-projectile-find-file)) ; y u no work?!

  ;; org helper keymap
  (bind-keys :map my-org-link-map
	     ("s" . org-store-link)
	     ("l" . org-insert-link))
  
  ;; org keymap
  (bind-keys :map my-org-major-mode-map
	     ("d" . org-deadline)
	     ("a" . org-agenda)
             ("s" . my-set-current-buffer-as-notes-file) ; s as in 'set'
             ("i" . org-redisplay-inline-images)
             ("f" . org-remove-inline-images) ; f as in 'fold'
             ("r" . pls-run-and-show)
             ("R" . org-babel-execute-src-block)
             ("g" . org-mark-ring-goto)
             ("m" . org-mark-ring-push)
             ("t" . org-toggle-checkbox)
             ("u" . org-update-checkbox-count)
             ("n" . org-toggle-narrow-to-subtree)
             ("o" . org-open-at-point)
	     ("l" . my-org-link-map))

  ;; elisp keymap
  (bind-keys :map my-elisp-major-mode-map
             ("e" . eval-last-sexp)
             ("n" . forward-sexp)
             ("p" . backward-sexp))

  ;; file manipulation keymap
  (bind-keys :map my-file-keymap
             ("f" . helm-find-files)
             ("r" . helm-recentf)
             ("l" . load-file)) ; TODO find helm alternative to this

  ;; buffer manipulation keymap
  (bind-keys :map my-buffer-keymap
             ("r" . pls-revert-buffer)
             ("b" . helm-buffers-list)
             ("k" . kill-this-buffer))

  ;; could spread them out as in
  ;; https://nextlevel-blog.de/emacs-for-academia-and-developers/ but
  ;; I prefer all of them being specified in one place, revisit later
  (evil-leader/set-key
    "<SPC>" 'helm-M-x
    "TAB" 'my-switch-to-last-buffer
    "g" my-magit-mode-map
    "f" my-file-keymap
    "b" my-buffer-keymap
    "t" 'neotree-toggle
    "d" 'helm-apropos ; 'd' as in 'describe'
    "w" my-window-keymap
    "p" my-projectile-mode-map
    ;; these must be handy and not burried somewhere
    "l" 'windmove-right
    "h" 'windmove-left
    "k" 'windmove-up
    "j" 'windmove-down
    "n" 'my-open-notes-file ; for opening the master notes file
    ;; keeping these for now...
    "[" 'backward-sexp
    "]" 'forward-sexp)

  (evil-leader/set-key-for-mode 'emacs-lisp-mode
    "m" my-elisp-major-mode-map)
  (evil-leader/set-key-for-mode 'org-mode
    "m" my-org-major-mode-map)
  ;; TODO this isn't working...

  ;; enabling evil 
  (use-package evil
    :init
    (evil-mode 1)
    :config
    (evil-set-undo-system 'undo-tree)
    ;; evil escape mode remaps 'fd' to escape for all evil things
    (use-package evil-escape
      :init
      (evil-escape-mode t)))
  ) ;; end use-package evil-leader

;; enable which key
(use-package which-key
  :init
  (which-key-mode))

;; enable helm
(use-package helm
  :init
  (helm-mode 1)
  :config
  (setq helm-buffer-max-length nil) ; adjusts buffer name column to match longest

  ;; these are a bit annoying by default, so adding them here
  ;; TODO is move all keybindings in one place, ofc
  (define-key helm-map (kbd "TAB") #'helm-execute-persistent-action)
  (define-key helm-map (kbd "<tab>") #'helm-execute-persistent-action)
  (define-key helm-map (kbd "C-z") #'helm-select-action)
  (define-key helm-map (kbd "C-j") #'helm-select-action)

  ;; this should make it so that helm always appears on the bottom
  ;; and indeed it does, but how :D
  ;; so TODO is figuring this thing out, can't even parse it
  (add-to-list 'display-buffer-alist
	       `(,(rx bos "*helm" (* not-newline) "*" eos)
		 (display-buffer-in-side-window)
		 (inhibit-same-window . t)
		 (window-height . 0.4)))
  )

;; does not seem to be working when loading init.el when emacs starts,
;; works if init.el is loaded with load-file, interesting
(use-package rainbow-delimiters
  :init
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  (rainbow-delimiters-mode t))

(use-package projectile
  :init
  (projectile-mode +1)

  ;; taken from
  ;; https://stackoverflow.com/questions/32513593/emacs-helm-projectile-slow-startup-and-projectile-file-find-very-slow
  ;; on a huge project, projectile-fined will hang otherwise
  (setq projectile-enable-caching t)

  (use-package helm-projectile)

  ;; TODO surely this can be done better
  (use-package neotree
    ;; TODO read complete neotree docs
    ;; https://www.emacswiki.org/emacs/NeoTree
    :init
    ;; neotree keymap
    (evil-define-key 'normal neotree-mode-map (kbd "TAB") 'neotree-enter)
    (evil-define-key 'normal neotree-mode-map (kbd "e") 'neotree-quick-look)
    ;; this doesn't work at all because it conflicts, crappy
    ;;(evil-define-key 'normal neotree-mode-map (kbd "r") 'neotree-change-root)
    (evil-define-key 'normal neotree-mode-map (kbd "J") 'neotree-change-root)
    (evil-define-key 'normal neotree-mode-map (kbd "K") 'neotree-select-up-node)
    ;;(evil-define-key 'normal neotree-mode-map (kbd "q") 'neotree-hide)
    (evil-define-key 'normal neotree-mode-map (kbd "RET") 'neotree-enter)
    (evil-define-key 'normal neotree-mode-map (kbd "g") 'neotree-refresh)
    ;;(evil-define-key 'normal neotree-mode-map (kbd "n") 'neotree-next-line)
    ;;(evil-define-key 'normal neotree-mode-map (kbd "p") 'neotree-previous-line)
    (evil-define-key 'normal neotree-mode-map (kbd "A") 'neotree-stretch-toggle)
    (evil-define-key 'normal neotree-mode-map (kbd "H") 'neotree-hidden-file-toggle)

    (setq projectile-switch-project-action 'neotree-projectile-action)
    ))

(use-package lsp
  :init
  (add-hook 'c++-mode-hook 'lsp)
  :config
  (setq lsp-clients-clangd-args
	'("-j=2"
	  "--background-index"
	  ;"--clang-tidy"
	  "--completion-style=bundled"
	  "--pch-storage=memory"
	  "--header-insertion=never"
	  "--header-insertion-decorators=0"))
  ;; this thing won't die ever
  (setq lsp-enable-indentation nil)
  ;; stop this horrible horrible feature from ever working
  (setq lsp-enable-on-type-formatting nil)

  ;; this promises to fix the insane indenting of lambdas in cppmode
  ;; see here:
  ;; https://stackoverflow.com/questions/23553881/emacs-indenting-of-c11-lambda-functions-cc-mode
  (c-set-offset 'inlambda 0) ; no extra indent for lambda
  (setq c-default-style "linux"
	c-basic-offset 4)
  
  ;;(c-offsets-alist . ((case-label . 0)
		      ;;(inline-open . 0)
                      ;;(substatement-open . 0)
                      ;;(block-open . 0) ; no space before {
                      ;;(knr-argdecl-intro . -)))
  )

(use-package toc-org
  :init
  (add-hook 'org-mode-hook 'toc-org-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;; General config ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; setup smoother scrolling because default is crap
(setq redisplay-dont-pause t
      scroll-margin 2
      scroll-step 1
      scroll-conservatively 10000
      scroll-preserve-screen-position 1

      ;; because screw trailing whitespace
      show-trailing-whitespace t

      ;; taken from: https://jamiecollinson.com/blog/my-emacs-config/ ;;;;;;
      ;; usually don't want tabs, if I do I can set this buffer-local to
      ;; t. If I just want one tab then use C-q (quoted-insert) to insert as
      ;; a literal.
      indent-tabs-mode nil

      ;; always give precedence to local settings for image width in orgmode
      org-image-actual-width nil

      ;; print column in modeline as well
      column-number-mode t

      ;; this adds automatic timestamps on task completion in orgmode
      org-log-done 'time

      ;; probably needed if I ever use it on windows or another stupidly
      ;; setup OS (disables bell)
      ring-bell-function 'ignore

      ;; check out
      ;; https://emacs.stackexchange.com/questions/41220/org-mode-disable-indentation-when-promoting-and-demoting-trees-subtrees
      ;; in short, don't indent paragraphs when indenting trees/subtrees
      org-adapt-indentation nil
      )

;; some visual stuff
(menu-bar-mode -1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)
;; highlight matching parenthesis
(show-paren-mode 1)
;; add orgmode hook for auto fill
(add-hook 'org-mode-hook 'turn-on-auto-fill)
;; pls add line numbers
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; call M-x untabify on the whole file (applies only to selection so
;; select everything first)

;; I like the ubuntu mono font but looks like crap in VM, WHY
;; font setup:
;; 1) download from google fonts or whatever
;; 2) move to .local/share/fonts folder (checked by default by system)
;; 3) run fc-cache -f -v to refresh font caches (force, verbose)
;; 4) run fc-list : family | grep -i "ubuntu"
;; 5) run command:
(when (member "Ubuntu Mono" (font-family-list))
  (set-face-attribute 'default nil :font "Ubuntu Mono" :height 105))
;; TODO tried the same thing with inconsolata but can't seem to find a
;; proper way :D

;; TODO: set alternate fonts, but usually default is great
;; use below code but tailor first:
;;(cond  ; executes first command where cond=true
;; ((find-font (font-spec :name "DejaVu Sans Mono"))
;;  (set-frame-font "DejaVu Sans Mono-12"))
;; ((find-font (font-spec :name "inconsolata"))
;;  (set-frame-font "inconsolata-12"))
;; ((find-font (font-spec :name "Lucida Console"))
;;  (set-frame-font "Lucida Console-12"))
;; ((find-font (font-spec :name "courier"))
;;  (set-frame-font "courier-12"))
;; (t "no specified fonts found, using default"))

;; always display fill column
(if (version< "27.0" emacs-version)
    (display-fill-column-indicator-mode))

;; use different themes for different computers, surely can be done better
(if (bound-and-true-p my-is-work-config)
    (load-theme 'solarized-gruvbox-dark t)
  (load-theme 'flatland))

;; taken from: https://jamiecollinson.com/blog/my-emacs-config/ ;;;;;;
;; should ensure that all backups are created in a single directory in
;; .emacs, otherwise they are spread out all over the filesystem TODO
;; is checking how this actually works
;;(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
;;      backup-by-copying t    ; Don't delink hardlinks
;;      version-control t      ; Use version numbers on backups
;;      delete-old-versions t  ; Automatically delete excess backups
;;      kept-new-versions 20   ; how many of the newest versions to keep
;;      kept-old-versions 5    ; and how many of the old
;; )

;; TODO not sure how this actually works
(org-link-set-parameters "info" :follow #'my-org-info-open-new-window)

;; setup plantuml if the path is defined
(if (bound-and-true-p org-plantuml-jar-path)
    (progn (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
        (org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t))))
    (my-trace "Plantuml not setup!"))

